<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <style type="text/css">
            .loader {
            border: 16px solid #f3f3f3; /* Light grey */
            border-top: 16px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 12px;
            height: 12px;
            animation: spin 2s linear infinite;
            display: none;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
        </style>
        <!-- Styles -->
       
    </head>
    <body>
            
        <div id="container">
          <h1>Houses</h1> 
          
            <div >
              <form>
                <input type="text" ref="inputName" placeholder="Name">
                <input type="number" ref="inputPrice1" placeholder="Price range minimum">-
                <input type="number" ref="inputPrice2" placeholder="Price range maximum">
                <input type="number" ref="inputBedrooms" placeholder="Bedrooms">
                <input type="number" ref="inputBathrooms" placeholder="Bathrooms">
                <input type="number" ref="inputStoreys" placeholder="Storeys">
                <input type="number" ref="inputGarages" placeholder="Garages">
                <button @click.prevent="getFormValues()">
                    <div class="loader"></div> Search
                </button>
              </form>
            </div>
            

          <table class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Price</th>
                <th>Bedrooms</th>
                <th>Bathrooms</th>
                <th>Storeys</th>
                <th>Garages</th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="room in rooms">
                <td>@{{room.id}}</td>
                <td>@{{room.name}}</td>
                <td>@{{room.price}}</td>
                <td>@{{room.bedrooms}}</td>
                <td>@{{room.bathrooms}}</td>
                <td>@{{room.storeys}}</td>
                <td>@{{room.garages}}</td>
              </tr>
              <td v-if="rooms.length == 0">No records found</td>
            </tbody>
          </table>  
        </div>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>

      <script type='text/javascript'>
   
         

        var v = new Vue({ 
          el: '#container',
          data: {
            rooms: [],
            output: ''
          },
          methods: {
            getFormValues () {
                this.inputName = this.$refs.inputName.value;
                $('.loader').show();
                
                $.post({
                    url: "http://localhost:8000/api/rooms/show", 
                    data:{
                        name:this.$refs.inputName.value, 
                        price1:this.$refs.inputPrice1.value, 
                        price2:this.$refs.inputPrice2.value, 
                        bathrooms:this.$refs.inputBathrooms.value, 
                        bedrooms:this.$refs.inputBedrooms.value, 
                        storeys:this.$refs.inputStoreys.value, 
                        garages:this.$refs.inputGarages.value
                    }, 
                    success:function(data){    
                        v.rooms = data.data;
                    },
                    complete: function(){
                        $('.loader').hide();
                    }
                });
            }
          },
          mounted: function(){
            $.post('http://localhost:8000/api/rooms/show', function(data){ 
              v.rooms = data.data;
            })}
        });

        

      </script>
    </body>
</html>
