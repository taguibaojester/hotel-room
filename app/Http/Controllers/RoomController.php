<?php

namespace App\Http\Controllers;

use App\Room;
use Illuminate\Http\Request;
use App\Http\Resources\Room as RoomResource;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
 
        $rooms = Room::where(function($query) use ($request) {

            if ($request->name) {
                $query->where('name', 'LIKE', "%$request->name%");
            }
            if ($request->price1) {
                if($request->price2==""){
                    $request->price2=$request->price1;
                }
                $query->whereBetween('price', [$request->price1, $request->price2]);       
            }
            if ($request->bedrooms) {
                $query->where('bedroom', $request->bedrooms);
            }
            if ($request->bathrooms) {
                $query->where('bathroom', $request->bathrooms);
            }
            if ($request->storeys) {
                $query->where('storey', $request->storeys);
            }
            if ($request->garages) {
                $query->where('garage', $request->garages);
            }

        })->get();

  
        return RoomResource::collection($rooms);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Room $room)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        //
    }
}
